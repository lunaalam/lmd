<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/send','SendController@index');

Route::Get('/home','IndexController@index');
Route::resource('movies', 'MoviesController');

Route::GET('comments/{id}','CommentsController@destroy');

Route::resource('comments', 'CommentsController');
Route::POST('comments/store/{id}','CommentsController@store');
Route::POST('comments/{id}/edit','CommentsController@edit');

Route::GET('admins','AdminsController@index');
Route::get('/admins/logout','AdminsController@logout');
Route::GET('admins/create','AdminsController@create');
Route::POST('admins','AdminsController@store');
Route::GET('admins/showusertable','AdminsController@showutable');
Route::GET('admins/showmovietable','AdminsController@showmtable');
Route::POST('admins/login','AdminsController@login');

Route::POST('/home',array(

'as'=>'language-chooser',
'uses'=>'IndexController@chooser'

	))->middleware('lang');
