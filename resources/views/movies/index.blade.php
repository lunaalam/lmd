@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<head>
		<title>Home Of Movies</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		{{ HTML::style('css/main.css') }}

	</head>



	<body style="background-color:#333333 " id="top">

			<!-- Banner -->
			<!--
				To use a video as your background, set data-video to the name of your video without
				its extension (eg. images/banner). Your video must be available in both .mp4 and .webm
				formants to work correctly.
			-->
		
				<section id="banner" data-video="images/banner">
					<div class="inner">
						<header>
							<h1>LMD</h1>
							<p>Luna's Movies Database <br />
							Home Of Movies.</p>
						</header>
						<a href="#main" class="more">Learn More</a>
					</div>
				</section>

			<!-- Main -->
				<div id="main">
					<div class="inner">

					<!-- Boxes -->
						<div class="thumbnails">

				 @foreach($movies as $key => $value)
				 @if(!$value->is_deleted)

							<div class="box">
						
								<a href="https://youtu.be/s6zR2T9vn2c" class="image fit">
								<img src="{{ URL::to('/uploads/' . $value->photo_name) }}" alt="" /></a>
								<div class="inner">
									<h3>{{ $value->name }}</h3>
									<p>
									 <h1>Type:{{ $value->type }}<br>
                                        Actors:{{ $value->actors }}<br>
                                        GeneralDescription:{{ $value->actors }}

									 </h1>
									


									</p>
									<a href="{{ url('/movies/'.$value->id) }}" class="" data-poptrox="youtube,300x400"><button style="background-color : rgb(209,163,20)">{{trans('grammer.watch')}}</button></a>
									
								</div>
							</div>
@endif
				    @endforeach

			


						

						</div>

					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					
				</footer>

		<!-- Scripts -->
		{{ HTML::script('js/jquery.min.js') }}
		{{ HTML::script('js/jquery.scrolly.min.js') }}
				{{ HTML::script('js/jquery.poptrox.min.js') }}
				{{ HTML::script('js/skel.min.js')}}
				{{ HTML::script('js/util.js') }}
				{{ HTML::script('js/main.js') }}

			
	</body>
</html>