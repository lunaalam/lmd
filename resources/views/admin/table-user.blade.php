    @extends('admin.layouts.adminlayout')


@section('content')

  <div class="row">
  
          <div class="col-md-12">
            <div class="card">
              <h3 class="card-title">Responsive Table</h3>
              <div class="table-responsive">
                <table class="table">
                  <thead>

                  
                    <tr>
                      <th>id</th>
                      <th> Name</th>
                      <th>Email</th>
                    </tr>
                  </thead>
                  <tbody>

                  @foreach($users as $user)
                    <tr>
                      <td>{{$user->id}}</td>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                    </tr>
                   @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      
@stop
        