    @extends('admin.layouts.adminlayout')


@section('content')

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
        <div class="page-title">
          <div>
            <h1>Movies Table</h1>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>{{trans('grammer.table')}}</li>
              <li class="active"><a href="#">{{trans('grammer.table-movie')}}</a></li>
            </ul>
          </div>
          <div><a class="btn btn-primary btn-flat" href="{{ URL::to('movies/create') }}"><i class="fa fa-lg fa-plus"></i></a>
       </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <table class="table table-hover table-bordered" id="sampleTable">
                  <thead>

                    <tr>
                      <th>Name</th>
                      <th>Actors</th>
                      <th>Type</th>
                      <th>Age</th>
                      <th>photo_name</th>
                      <th>vedio_name</th>
                    </tr>
                  </thead>
                  <tbody>

                  @if($movies)
                                 @foreach($movies as $movie)
                                 @if($movie->is_deleted!=1)

                   <tr>
                      <td>{{$movie->name}}</td>
                      <td>{{$movie->actors}}</td>
                      <td>{{$movie->type}}</td>
                      <td>{{$movie->content}}</td>
                      <td>{{$movie->photo_name}}</td>
                      <td>{{$movie->vedio_name}}</td>
                      <td><a  href= "{{ URL::to('movies/' . $movie->id . '/edit') }}">{{trans('grammer.edit')}}</a></td>
                                 <td><a  href= "{{ URL::to('movies/'. $movie->id) }}">{{trans('grammer.delete')}}</a></td>
           
                    </tr>
                    @endif
                    @endforeach

            @endif
              
                    
                 
                   
               
                  
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
   
    <!-- Javascripts-->


{{ HTML::script('js/plugins/jquery.dataTables.min.js') }}
{{ HTML::script('js/plugins/dataTables.bootstrap.min.js') }}

    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@stop