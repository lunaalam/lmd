<?php 


return array(
'admin'=>'الادمن',
'login'=>'تسجيل الدخول',
'hello'=>'مرحيا',
'watch'=>'شاهد',
'table'=>'لجداول',
'dash'=>'صفحتي',
'pages'=>'الصفحات',
'users'=>'المستخدمين',
'table-movie'=>'جدول الافلام',
'table-user'=>'جدول المستخدمين ',
'delete'=>'حذف',
'edit'=>'تعديل ',
'name'=>'الاسم',
'des'=>'الوصف',
'pt'=>'الصورة والفيدسو',
'submit'=>'تأكيد',
'moviename'=>'اسم الفيلم',
'moviehero'=>'ابطال الفيلم',
'movietype'=>'نوع الفيلم',
'generaldes'=>'وصف عام',
'moviepost'=>'بوست الفيلم',
'movievedio'=>'فيديو الفيلم',
'add'=>'اضافة فيلم',
'comments'=>'التعليقات',
'send'=>'ارسال',

	);