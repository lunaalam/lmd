<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\models\Admin;
use DB;
use Session;
use App\Mail\SendMail;
use View;
use App\models\Comment;
use App\models\User;
use App\models\Movie;


class AdminsController extends Controller

{
  public

  function index()
  {
    app()->setlocale(Session::get('locale'));
    return View::make('admin.login');
  }

  public

  function create()
  {
    app()->setlocale(Session::get('locale'));
    return View::make('admin.register');
  }

  public

  function store()
  {
    if (Session::get('Admin_id'))
    {

      // store

      $admin = new Admin;
      $admin->name = Input::get('name');
      $admin->email = Input::get('email');
      $admin->level = Input::get('level');
      $admin->password = Input::get('password');
      $admin->save();

      // redirect

      Session::flash('message', 'Successfully created admin!');
      return Redirect::to('admins');
    }
    else
    {
      Session::flash('message', 'Not logged in!');
      return Redirect::to('admins');
    }
  }

  public

  function login()
  {
    $email = Input::get('email');
    $pass = Input::get('password');
    $admin = DB::table('admins')->where('email', $email)->where('password', $pass)->first();

    // $admin=Admin::where('email','password',$name,$pass )->get();

    if ($admin)
    {
      Session::put('Admin_id', $admin->id);
       Session::put('Admin_email', $admin->email);
      app()->setlocale(Session::get('locale'));
      return View::make('admin.home');
    }
    else
    {
      Session::flash('message', 'Not Admin!');
      return Redirect::to('admins');
    }
  }

  public

  function showutable()
  {
    if (Session::get('Admin_id'))
    {
      $users = User::all();
      app()->setlocale(Session::get('locale'));
      return View::make('admin.table-user')->with('users', $users);
    }
    else
    {
      Session::flash('message', 'Not logged in!');
      return Redirect::to('admins');
    }
  }

  public

  function showmtable()
  {
    if (Session::get('Admin_id'))
    {
      $movies = Movie::all();
      app()->setlocale(Session::get('locale'));
      return View::make('admin.table-movie')->with('movies', $movies);
    }
    else
    {
      Session::flash('message', 'Not logged in!');
      return Redirect::to('admins');
    }
  }

  public

  function logout()
  {
    Session::flush();
    return Redirect::to('admins');
  }
}