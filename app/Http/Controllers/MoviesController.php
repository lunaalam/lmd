<?php

namespace App\Http\Controllers;
use Validator;
use App\Http\Requests\movie_form_validation;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\models\Movie;
use App\models\Comment;
use App\models\User;

use View;
use Request;
use Auth;
use Carbon;


class MoviesController extends Controller
{
    	  public function index()
    { 
      if(Auth::id()){

  
  $movies = Movie::all();

  app()->setlocale(Session::get('locale'));


        return View::make('movies.index')
            ->with('movies', $movies);
}else {

        return View::make('Auth.login');


}


    }


      public function create()
    {

      if(Session::get('Admin_id')){

        app()->setlocale(Session::get('locale'));

        return View::make('movies.create');
    }

else {

Session::flash('message', 'Not Admin!');
            return Redirect::to('admins');

}


  }


      public function store(movie_form_validation $rules)
    {
            $movie=new Movie;

		
		

                 
        if(Request::hasFile('vedio')){

            $file = Request::file('vedio');
              $filename = $file->getClientOriginalName();
            $path = public_path().'/uploads/';
        $file->move($path, $filename);
              $movie->vedio_name=$filename;

        }


 if(Request::hasFile('image')){

            $file = Request::file('image');
              $filename = $file->getClientOriginalName();
            $path = public_path().'/uploads/';
        $file->move($path, $filename);
              $movie->photo_name= $filename;

        }

     $movie->name= Input::get('name');
     $movie->content=Input::get('content');
     $movie->actors=Input::get('actors');
     $movie->type=Input::get('type');
    ///$movie->is_deleted=false;

            $movie->save();





       Session::flash('message', 'Successfully created Movie!');
                   return Redirect::to('admins/showmovietable');

        



    }



        public function edit($id)
    {
      if(Session::get('Admin_id')){





        


        $movie = Movie::find($id);
if(!$movie) {
  
              Session::flash('message', 'No Movie Found!!');

              return Redirect::to('movies');
  
}
else{

    $Admin_id=$movie->created_by;
            $logged_in=Session::get('Admin_id'); 


            if($Admin_id==$logged_in){


  app()->setlocale(Session::get('locale'));

        return View::make('movies.edit')
            ->with('movie', $movie);
}


else{

   Session::flash('message', 'Not youre Movie!');

                   return Redirect::to('admins/showmovietable');

}
    }


}
else {Session::flash('message', 'Not Admin!');
            return Redirect::to('admins');}
}
        public function update($id)
    {
        
 
    
       
            $movie = Movie::find($id);
            if(!$movie){


          Session::flash('message', 'No Movie Found !');
            return Redirect::to('admins/showmovietable');
            }
              else{
            $movie->name       = Input::get('name');
            $movie->type      = Input::get('type');
            $movie->content = Input::get('content');
                        $movie->actors = Input::get('actors');
                        if(Input::get('image' )){
           $movie->photo_name = Input::get('image' );


                        }
                        
                    if(Input::get('vedio' )){
           $movie->photo_name = Input::get('vedio' );


                        }


            $movie->save();

            // redirect
            Session::flash('message', 'Successfully updated movie!');
            return Redirect::to('admins/showmovietable');}
        }
    


    public function show($id){

      $users=array();
$users=array();
    $movie=Movie::find($id);
    $comments=Comment::where('vedio_id',$id)->get();



   if(!$movie) {
  
              Session::flash('message', 'No Movie Found!!');

              return Redirect::to('movies');
  
}
else{
    app()->setlocale(Session::get('locale'));

     
      return View('movies.full' , compact('comments', 'movie','users'));
   
}


    }




    public function destroy($id){

$movie = Movie::find($id);

if(!$movie){

 Session::flash('message', 'No movie found!');
            return Redirect::to('admins/showmovietable');
}
else {
$Admin_id=$movie->created_by;
$logged_in=Session::get('Admin_id');

       if($Admin_id==$logged_in){

$movie->is_deleted=1;
$movie->deleted_at=Carbon\Carbon::now();
$movie->deleted_by=Session::get('Admin_id');;
$movie->save();

     Session::flash('message', 'Movie deleted!');
            return Redirect::to('admins/showmovietable');
}

else 

        { Session::flash('message', 'Not Yore Movie!');
  return Redirect::to('admins/showmovietable');

    }





}
}
}