<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Redirect;
use Carbon;
use View;
use App\models\Comment;
use Auth;
class CommentsController extends Controller

{
    public

    function index()
    {
    }

    public

    function store($movid)
    {
        $comment = new Comment;

        $comment->content = Input::get('content');
        $comment->vedio_id = $movid;
        $comment->save();

        // redirect

        Session::flash('message', 'Successfully created comment!');
        return Redirect::to('movies/'.$movid);
    }

    public

    function edit($id)
    {
        $comment = Comment::find($id);
        $user_id = $comment->created_by;
        $logged_in = Auth::id();
        if ($user_id == $logged_in)
        {
            $comment->content = Input::get('content');
            $movid = $comment->vedio_id;
            $comment->save();

            // redirect

            Session::flash('message', 'Successfully updated comment!');
            return Redirect::to('movies/'. $movid);
        }
        else
        {
            Session::flash('message', 'Not Yore Comment!');
            return Redirect::to('movies/'. $comment->vedio_id);
        }
    }

    public

    function destroy($id)
    {
        $comment = Comment::find($id);
        $user_id = $comment->created_by;
        $logged_in = Auth::id();
        if ($user_id == $logged_in)
        {
            $comment->is_deleted = 1;
            $comment->deleted_at = CarbonCarbon::now();
            $comment->deleted_by = Auth::id();
            $comment->save();
            Session::flash('message', 'Comment deleted!');
            return Redirect::to('movies/show/' . $comment->vedio_id);
        }
        else
        {
            Session::flash('message', 'Not Yore Comment!');
            return Redirect::to('movies/show/' . $comment->vedio_id);
        }
    }
}

