<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class movie_form_validation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
       
            'name'       => 'required',
            'vedio'      => 'required|mimes:mp4,wmv,mov,banner ',
            'image' => 'required|mimes:jpeg,bmp,png,gif,jpg',
             'type' => 'required',
            'content' => 'required',
            'actors' => 'required',
   


      
        ];
    }
}
