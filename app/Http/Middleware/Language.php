<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class Language
{
    public function handle($request, Closure $next)
    {
   //app()->setlocale(Session::get('locale'));

            App::setLocale(Session::get('locale'));
 
        return $next($request);
    }
}