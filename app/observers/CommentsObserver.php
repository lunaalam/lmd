<?php

namespace App\Observers;
use App\models\Comment;
use App\models\User;
use Mail;

class   CommentsObserver{

	public function creating (Comment $comment){


         $comment->created_by=\Auth::id();


         	 Mail::raw('comments added', function ($message){
            $message->to(\Auth::user()->email);
 });
 


	}


	public function updating (Comment $comment){

         $comment->updated_by=\Auth::id(); 

            	 Mail::raw('comments updated', function ($message){
            $message->to(\Auth::user()->email);
 });
 


}

}
?>