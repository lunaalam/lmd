<?php

namespace App\Observers;
use App\models\Movie;
use App\models\User;
use App\models\Admin;
use Session;
use Mail;


class   MoviesObserver{

	public function creating (Movie $movie){
  $admin_id=Session::get('Admin_id');

         $movie->created_by= $admin_id; 

      Mail::raw('Movie Created', function ($message){
            $message->to(Session::get('Admin_email'));
 });

	}

	public function updating (Movie $movie){
  $admin_id=Session::get('Admin_id');

         $movie->updated_by= $admin_id; 

               Mail::raw('Movie updated', function ($message){
            $message->to(Session::get('Admin_email'));
 });



	}


}


?>