<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Comment;

class CommentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//\App\Observers\CommentsObserver::class);


        \App\Comment::creating(function($Comment){

            $Comment->created_by=0;


        });

            }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
