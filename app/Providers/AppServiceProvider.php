<?php

namespace App\Providers;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\ServiceProvider;
use Session;
use App;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()


    {  


             Schema::defaultStringLength(191);


        \App\models\Comment::observe(\App\Observers\CommentsObserver::class);
                \App\models\Movie::observe(\App\Observers\MoviesObserver::class);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
