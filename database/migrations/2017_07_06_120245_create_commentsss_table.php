<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->integer('vedio_id');

            $table->integer('deleted_by')->unsigned()->nullable();

            $table->integer('created_by')->unsigned();

            $table->integer('updated_by')->unsigned()->nullable();
                        $table->boolean('is_deleted')->default(false);




            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes(); 
    


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('Comments');

    }
}
