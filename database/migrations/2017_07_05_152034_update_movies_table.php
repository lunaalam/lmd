<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                  Schema::table('Movies', function ($t) {
                $t->string('actors');
                $t->string('content');

             $t->integer('created_by')->unsigned();
            $t->foreign('created_by')->references('id')->on('admins');

             $t->integer('deleted_by')->unsigned()->nullable();;
                     $t->foreign('deleted_by')->references('id')->on('admins');

                        $t->integer('updated_by')->unsigned()->nullable();
                       $t->foreign('updated_by')->references('id')->on('admins');






        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::table('Movies', function ($t) {

                $t->dropColumn('content','actors');

                       $t->dropForeign('Movies_created_by_foreign');
                        $t->dropColumn('created_by');

                         $t->dropForeign('Movies_deleted_by_foreign');
                        $t->dropColumn('deleted_by');

                         $t->dropForeign('Movies_updated_by_foreign');
                        $t->dropColumn('updated_by');

                


        });
    }
}
