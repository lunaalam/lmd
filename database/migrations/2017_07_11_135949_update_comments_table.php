<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('Comments', function ($table) {

              $table->foreign('created_by')->references('id')->on('users');

                     $table->foreign('deleted_by')->references('id')->on('users');

                       $table->foreign('updated_by')->references('id')->on('users');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
              Schema::table('Movies', function ($t) {


                       $t->dropForeign('Comments_created_by_foreign');

                         $t->dropForeign('Comments_deleted_by_foreign');

                         $t->dropForeign('Comments_updated_by_foreign');

                


        });
    }
}
