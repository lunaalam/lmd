<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('Movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('photo_name');
            $table->string('vedio_name');
                            $table->string('type');

      


            $table->boolean('is_deleted')->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes(); 



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('Movies');

    }
}
